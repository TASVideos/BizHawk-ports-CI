{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/nixos-21.05.tar.gz") { }
, stdenv ? pkgs.stdenv
, scons ? pkgs.scons
, zlib ? pkgs.zlib
}:
rec {
	Cygne = stdenv.mkDerivation {
		pname = "bizhawk-core-cygne";
		version = "2.6.2+8adb91162";
		src = mainRepo;
		buildInputs = [ stdenv ];
		dontConfigure = true;
		buildPhase = ''
			set -e
			cd wonderswan/make
			make
		'';
		installPhase = ''
			set -e
			mkdir -p $out/usr/lib/bizhawk/internal
			make install
			install -m644 bizswan.dll $out/usr/lib/bizhawk/internal/libbizswan.dll.so
		'';
	};
	Gambatte = stdenv.mkDerivation {
		pname = "bizhawk-core-gambatte";
		version = "2.6.2+8adb91162";
		src = ./Gambatte;
		buildInputs = [ scons stdenv zlib ];
		dontConfigure = true;
		buildPhase = ''
			set -e
			cd submodules/gambatte
			scripts/build_shlib.sh
		'';
		installPhase = ''
			set -e
			mkdir -p $out/usr/lib/bizhawk/internal
			install -m644 ../../Assets/dll/libgambatte.so $out/usr/lib/bizhawk/internal/
		'';
	};
	Handy = stdenv.mkDerivation {
		pname = "bizhawk-core-handy";
		version = "2.6.2+8adb91162";
		src = mainRepo;
		buildInputs = [ stdenv ];
		dontConfigure = true;
		buildPhase = ''
			set -e
			cd lynx/make
			make
		'';
		installPhase = ''
			set -e
			mkdir -p $out/usr/lib/bizhawk/internal
			make install
			install -m644 bizlynx.dll $out/usr/lib/bizhawk/internal/libbizlynx.dll.so
		'';
	};
	mainRepo = ./BizHawk;
	mGBA = stdenv.mkDerivation {
		pname = "bizhawk-core-mgba";
		version = "2.6.2+8adb91162";
		src = ./mGBA;
		buildInputs = [ stdenv ];
		dontConfigure = true;
		buildPhase = ''
			set -e
			cd submodules/mgba/src/platform/bizhawk/linux
			make
		'';
		installPhase = ''
			set -e
			mkdir -p $out/usr/lib/bizhawk/internal
			make install
			install -m644 libmgba.dll.so $out/usr/lib/bizhawk/internal/libmgba.dll.so
		'';
	};
	QuickNes = stdenv.mkDerivation {
		pname = "bizhawk-core-quicknes";
		version = "2.6.2+8adb91162";
		src = mainRepo;
		buildInputs = [ stdenv ];
		dontConfigure = true;
		buildPhase = ''
			set -e
			cd quicknes/make
			make
		'';
		installPhase = ''
			set -e
			mkdir -p $out/usr/lib/bizhawk/internal
			make install
			install -m644 libquicknes.dll $out/usr/lib/bizhawk/internal/libquicknes.dll.so.0.7.0
		'';
	};
}
